package com.company.isassignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello Things");

        double lr = .5;

        //lets start trying to program some logic
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/Users/matthewmartin/Documents/Documents - Matthew’s MacBook Pro/Uni/IS/Assignment 2 - Reconition/src/com/company/isassignment/Patterns/PatternFiles/Clock.txt"));
            String line;
            String bigLine = "";

            while ((line = br.readLine()) != null) {
                bigLine = bigLine + line;
            }

            //hidden layer neutron
            Neuron hl = new Neuron();
            hl.setDescription("Hidden Layer ");

            //insert values into synapses
            Synapse[] inputs = new Synapse[144];

            for(int i = 0; i < 144; i++){
                int value = 0;

                if (bigLine.charAt(i) == '*') value = 1;

                inputs[i] = new Synapse();
                inputs[i].setValue(value);
                inputs[i].setWeight(1);
                inputs[i].setOutput(hl);
            }

            // set these inputs to the hidden layer
            hl.setInputs(inputs);

            //lets set up our output neuron
            Neuron[] outputs = new Neuron[12];
            Synapse[] outputSynapses = new Synapse[12];

            for(int i = 0; i < 12; i++){
                outputSynapses[i] = new Synapse();
                outputSynapses[i].setInput(hl);

                //create neuron
                outputs[i] = new Neuron();
                outputs[i].setDescription("Output " + i + " ");

                //create input list
                Synapse[] inputsForNeuron = {outputSynapses[i]};
                outputs[i].setInputs(inputsForNeuron);

                outputSynapses[i].setOutput(outputs[i]);
            }

            //setting the desired output for this shape
            outputs[0].setDesiredOutput(1.0);

            hl.setOutputs(outputSynapses);

            //set fire that neuron
            hl.fire();

            //command the back tracking to happen
            System.out.println("Back Tracking");
            hl.backTrack(lr);

            //get the result again
            hl.fire();

        }catch (IOException e) {
            System.out.print(e);
        }
    }
}
