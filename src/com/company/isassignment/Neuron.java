package com.company.isassignment;

/**
 * Created by matthewmartin on 19/09/2016.
 * What bridge should i jump off
 */
public class Neuron {
    private Synapse[] inputs;
    private Synapse[] outputs;
    private double desiredOutput = 0;
    private double value;
    private String description;

    public double activationFunction(double sum){
        return (1/( 1 + Math.pow(Math.E,(-1*sum))));
    }

    public void setInputs(Synapse[] inputs){
        this.inputs = inputs;
    }

    public Synapse[] getInputs(){
        return this.inputs;
    }

    public void setOutputs(Synapse[] outputs){
        this.outputs = outputs;
    }

    public Synapse[] getOutputs(){
        return this.outputs;
    }

    public void setDescription(String desc){
        this.description = desc;
    }

    public void setDesiredOutput(double desiredOutput){
        this.desiredOutput = desiredOutput;
    }

    public double getDesiredOutput(){
        return desiredOutput;
    }

    public double getValue(){
        return value;
    }

    public void fire(){
        //get the sum of the inputs
        for(int i = 0; i < this.inputs.length; i++){
            this.value += this.inputs[i].getValue();
        }

        //apply the activation function
        this.value = activationFunction(this.value);

        //printing value to test, delete this
        System.out.println(this.description + "'s value is " + this.value);

        //if the Neuron has no children it must be an output, so well print it out, should probably do it better, but you know fuckit
        if(this.outputs == null){
            System.out.println(this.description + "was a final neuron, it's value is" + this.value);
        }

        else {
            //sets outputs to this value
            for (int i = 0; i < this.outputs.length; i++) {
                this.outputs[i].setValue(this.value);
                this.outputs[i].getOutput().fire();
            }
        }
    }

    public void backTrack(double lr){
        // we want to call back track on any child values
        if(this.outputs != null){
            //sets outputs to this value
            for (int i = 0; i < this.outputs.length; i++) {
                this.outputs[i].getOutput().backTrack(lr);
                this.outputs[i].backTrack(lr);
            }
        }

        for(int i = 0; i < inputs.length; i++){
            if(inputs[i].getInput() == null){
                inputs[i].backTrack(lr);
            }
        }
    }

}
